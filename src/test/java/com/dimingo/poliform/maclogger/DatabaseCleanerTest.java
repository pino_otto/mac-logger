package com.dimingo.poliform.maclogger;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.assertNotNull;


public class DatabaseCleanerTest {

    private DatabaseCleaner databaseCleaner;

    @Before
    public void setUp() {

        // load the spring application context
        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("applicationContext.xml");

        // get the alarm logger bean
        databaseCleaner =
                (DatabaseCleaner) applicationContext.getBean("databaseCleaner");

    } // end setUp


    @Test
    public void testDatabaseCleanerCreated() {

        assertNotNull(databaseCleaner);

    } // end testDatabaseCleanerCreated

}
