package com.dimingo.poliform.maclogger;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.assertNotNull;


public class AlarmLoggerTest {

    private AlarmLogger alarmLogger;

    @Before
    public void setUp() {

        // load the spring application context
        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("applicationContext.xml");

        // get the alarm logger bean
        alarmLogger =
                (AlarmLogger) applicationContext.getBean("alarmLogger");

    } // end setUp


    @Test
    public void testAlarmLoggerCreated() {

        assertNotNull(alarmLogger);

    } // end testAlarmLoggerCreated

}
