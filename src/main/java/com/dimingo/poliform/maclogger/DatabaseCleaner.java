package com.dimingo.poliform.maclogger;

import com.dimingo.poliform.mac.commons.dao.IAlarmDao;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;

public class DatabaseCleaner {

    private final static Logger logger = Logger.getLogger(DatabaseCleaner.class);

    /** --------------------------------------------------------------------------------------------
     * Spring injection (values and collaborators)
     */

    private IAlarmDao alarmDao;

    /**
     * deleteOldAlarms
     */
//    @Scheduled(cron = "0 0 4 * * ?") // prod: every day at 4:00 am
//    @Scheduled(cron = "*/5 * * * * ?") // test 1: every 5 seconds
    @Scheduled(cron = "0 0 * * * ?") // test 2: every hour
    public void deleteOldAlarms() {

        long now = System.currentTimeMillis() / 1000;
        logger.info("****************************************************************************");
        logger.info("********************************************************** start to clean DB");

        alarmDao.deleteOldAlarms();

        logger.info("********************************************************** ended to clean DB");
        logger.info("****************************************************************************");

    }


    /**
     * Setter and Getters
     */

    public void setAlarmDao(IAlarmDao alarmDao) {
        this.alarmDao = alarmDao;
    }

}
