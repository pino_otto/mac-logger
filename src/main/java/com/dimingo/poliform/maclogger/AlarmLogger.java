package com.dimingo.poliform.maclogger;

/*
 * Structure of the modbus holding registers for the MAC device
 * ============================================================
 * 
 * Each address refers to a word (2 bytes) register.
 * Only the least significant byte (LSB) of the register is used.
 * 
 * Here is the register mapping:
 * 
 *   address	contents (LSB)
 *   -------    -------------------------------------------------------
 *   0x0A		green_01..green_08
 *   0x0B		green_09..green_16
 *   0x0C		green_17..green_24
 *   0x0D		green_25..green_30 (two least significant bits not used)
 *   0x0E		red_01..red_08
 *   0x0F		red_09..red_16
 *   0x10		red_17..red_24
 *   0x11		red_25..red_30 (two least significant bits not used)
 *   0x12		anom_01 (only least significant bit used)
 *   0x13		anom_02 (only least significant bit used)
 *   0x14		anom_03 (only least significant bit used)
 *   0x15		anom_04 (only least significant bit used)
 *   0x16		reset (only least significant bit used)
 *   0x17		mute (only least significant bit used)
 *   
 *    
 */

import com.dimingo.modbus.hao.serial.IModbusSerialDriver;
import com.dimingo.modbus.hao.tcp.IModbusTcpDriver;
import com.dimingo.poliform.mac.commons.bean.Alarm;
import com.dimingo.poliform.mac.commons.bean.Command;
import com.dimingo.poliform.mac.commons.bean.Device;
import com.dimingo.poliform.mac.commons.dao.IAlarmDao;
import com.dimingo.poliform.mac.commons.dao.ICommandDao;
import com.dimingo.poliform.mac.commons.dao.IDeviceDao;
import com.dimingo.poliform.maclogger.bean.SlaveGroup;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;



public class AlarmLogger implements ApplicationContextAware {
	
	private final static Logger logger = Logger.getLogger(AlarmLogger.class);

	private final static int ZERO = 0;
	
	/*
	 * Application context awareness
	 */
	
	private ApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
	
	/** --------------------------------------------------------------------------------------------
	 * Spring injection (values and collaborators)
	 */
	
	private IAlarmDao alarmDao;
	
	private ICommandDao commandDao;
	
	private IDeviceDao deviceDao;
	
	private int masterId = 100;
	
	private int startAddress = 10; 			// zero based, overwritten by Spring configuration
	
	private int numberOfRegisters = 14;		// overwritten by Spring configuration
	
	private long sleepTime = 500; 			// milliseconds
	
	
	/**
	 * Private variables
	 */
	
	private int resetAddressOffset = 12;
	
	private int muteAddressOffset = 13;
	
	private IModbusSerialDriver modbusSerialDriver;

	private IModbusTcpDriver modbusTcpDriver;
	
	private HashMap<String, SlaveGroup> modbusDriverMap = new HashMap<String, SlaveGroup>();

	private InetAddressValidator inetAddressValidator = InetAddressValidator.getInstance();

	
	/**
	 * Default constructor
	 */
	public AlarmLogger() {
		
	} // end constructor
	
	
	/**
	 * execute
	 */
	public void execute() {
		
		logger.info("start executing...");
		logger.info("startAddress = " + startAddress);
		logger.info("numberOfRegisters = " + numberOfRegisters);

		/* ----------------------------------------------------------------------
		 * build the map for the slave groups
		 */
		
		// get the list of devices
		List<Device> deviceList = deviceDao.findAllDevices();
		
		// define the serial port names
		String serialPortList = "";
		
		// loop on the device list for setting the serial port property
		for (Device device : deviceList) {

			// get the serial port name
			String serialPort = device.getSerialPort();

			// check if it is a serial port
			if (isSerialPort(serialPort)) {

				// check if it is a new serial port
				if (!serialPortList.contains(serialPort)) {

					serialPortList += serialPort + ":";

				} // end

			}
			
		} // end for
		
		logger.debug("found the following serial ports: " + serialPortList);
		
		// set the system property for recognizing the serial port names
		System.setProperty("gnu.io.rxtx.SerialPorts", serialPortList);

		logger.debug("start to create the modbus drivers for the devices");
		
		// loop on the device list for creating the modbus drivers
		for (Device device : deviceList) {

			byte slaveId = device.getSlaveId();
			String deviceStatus = device.getStatus();

			logger.debug("--- device: " + slaveId + ", status = " + deviceStatus);

			if (deviceStatus.equals("0")) {
				logger.debug("device not enabled, it will be ignored");
				continue;
			}

			// get the connection type of the device
			String connectionType = getConnectionType(device);

			logger.debug("connectionType = " + connectionType);

			if (connectionType.equals("serial")) {

				// get the serial port name
				String serialPort = device.getSerialPort();

				logger.debug("processing serial port = " + serialPort);

				// check if the serial port is already in the hash map
				if (modbusDriverMap.containsKey(serialPort)) { // serial port exists

					logger.debug("a slave group is already existing for serial port: " + serialPort);

					// get the slave group
					SlaveGroup slaveGroup = modbusDriverMap.get(serialPort);

					// get the list of devices
					List<Device> devices = slaveGroup.getDeviceList();

					// add the device to the list of devices
					devices.add(device);

					logger.debug("added slave id = " + slaveId + " for serial port = " + serialPort);

				} else { // serial port does NOT exist

					logger.debug("create a new slave group for serial port: " + serialPort);

					// create a new modbus serial driver
					modbusSerialDriver = (IModbusSerialDriver) applicationContext.getBean("modbusSerialDriver");

					// set the serial port on the driver
					modbusSerialDriver.getSerialParameters().setPortName(serialPort);

					try {

						// open a serial modbus connection
						modbusSerialDriver.openConnection();

						logger.debug("opened a serial modbus connection SUCCESSFULLY on serial port " + serialPort);

					} catch (Exception e) {

						logger.error("UNABLE to open a serial modbus connection on serial port " + serialPort, e);

//						e.printStackTrace();

					} // end catch

					// create a new list of devices
					List<Device> devices = new ArrayList<Device>();

					// add the device to the list
					devices.add(device);

					// create a new slave group
					SlaveGroup slaveGroup = new SlaveGroup();

					// set the connection type in the slave group
					slaveGroup.setConnectionType("serial");

					// set the modbus serial driver in the slave group
					slaveGroup.setModbusSerialDriver(modbusSerialDriver);

					// set the list of devices in the slave group
					slaveGroup.setDeviceList(devices);

					// add the serial port with its slave group to the hash map
					modbusDriverMap.put(serialPort, slaveGroup);

					logger.debug("added slave id = " + slaveId + " for serial port = " + serialPort);

				} // end else

			} else if (connectionType.equals("tcp")) {

				// get the ip address and port
				String ipAddress = device.getIpAddress();
				String ipPort = device.getIpPort();

				String tcpPort = ipAddress + ":" + ipPort;
				logger.debug("processing tcp address:port = " + tcpPort);

				// check if the tcp port is already in the hash map
				if (modbusDriverMap.containsKey(tcpPort)) { // tcp port exists

					logger.debug("a slave group is already existing for tcp port: " + tcpPort);

					// get the slave group
					SlaveGroup slaveGroup = modbusDriverMap.get(tcpPort);

					// get the list of devices
					List<Device> devices = slaveGroup.getDeviceList();

					// add the device to the list of devices
					devices.add(device);

					logger.debug("added slave id = " + slaveId + " for tcp port = " + tcpPort);

				} else { // tcp port does NOT exist

					logger.debug("create a new slave group for tcp port: " + tcpPort);

					// create a new modbus tcp driver
					modbusTcpDriver = (IModbusTcpDriver) applicationContext.getBean("modbusTcpDriver");

					// set the tcp port on the driver
					modbusTcpDriver.setSlaveIpAddress(ipAddress);
					modbusTcpDriver.setSlavePort(Integer.parseInt(ipPort));

					try {

						// open a tcp modbus connection
						modbusTcpDriver.openConnection();

						logger.debug("opened a tcp modbus connection SUCCESSFULLY on tcp port " + tcpPort);

					} catch (Exception e) {

						logger.error("UNABLE to open a tcp modbus connection on tcp port " + tcpPort, e);

//						e.printStackTrace();

					} // end catch

					// create a new list of devices
					List<Device> devices = new ArrayList<Device>();

					// add the device to the list
					devices.add(device);

					// create a new slave group
					SlaveGroup slaveGroup = new SlaveGroup();

					// set the connection type in the slave group
					slaveGroup.setConnectionType("tcp");

					// set the modbus tcp driver in the slave group
					slaveGroup.setModbusTcpDriver(modbusTcpDriver);

					// set the list of devices in the slave group
					slaveGroup.setDeviceList(devices);

					// add the tcp port with its slave group to the hash map
					modbusDriverMap.put(tcpPort, slaveGroup);

					logger.debug("added slave id = " + slaveId + " for tcp port = " + tcpPort);

				} // end else

			}
			
		} // end for
		
		/* ----------------------------------------------------------------------
		 * infinite loop on the slave groups
		 */
		
		// get the list of slave groups
		Collection<SlaveGroup> slaveGroupList = modbusDriverMap.values();
		
		// loop forever
		while (true) {
		
			// process each slave group
			for (SlaveGroup slaveGroup : slaveGroupList) {
				
//				SerialConnection serialConnection = null;
				
				try {
					
//					// get the modbus serial driver
//					modbusSerialDriver = slaveGroup.getModbusSerialDriver();
//
//					// set master id
//					modbusSerialDriver.setMasterIdentifier(masterId);
//
//					// get the serial modbus connection
//					serialConnection = modbusSerialDriver.getSerialConnection();
//
//					logger.debug("get the modbus connection on serial port: " +
//								 modbusSerialDriver.getSerialParameters().getPortName());
//
//					// check for existing serial connection
//					if (serialConnection != null) {
					
					// define the array for storing all the holding registers (read)
					int[] holdingRegisters = new int[numberOfRegisters];

//					// define the array for storing only the reset and mute holding registers (write)
//					int[] resetAndMuteHoldingRegisters = new int[2];

//					// define the array for storing only the reset holding register (write)
//					int[] resetHoldingRegister = new int[1];
//
//					// define the array for storing only the mute holding register (write)
//					int[] muteHoldingRegister = new int[1];

					// define the previous alarm
					Alarm previousAlarm;

					// define the current alarm
					Alarm currentAlarm;

					// define the command
					Command command;

					// define the slave id
					Byte slaveId = null;

					// get the device list for the current slave group
					deviceList = slaveGroup.getDeviceList();

					// process each device
					for (Device device : deviceList) {

						try {

							// get the slave id
							slaveId = device.getSlaveId();

							logger.debug("---------------");
							logger.debug("===> processing slave id: " + slaveId);

							// get the last alarm from the database
							previousAlarm = alarmDao.findLastAlarmBySlaveId(slaveId);

							logger.info("previousAlarm = " + previousAlarm);

							// read the holding registers from the slave device
							holdingRegisters =
//									modbusSerialDriver.readHoldingRegisters(
//															slaveId,
//															startAddress - 1,
//															numberOfRegisters
//															);
								readHoldingRegisters(slaveGroup, slaveId);

							// create a new alarm object with the data read from the slave device
							currentAlarm = createAlarm(slaveId, holdingRegisters);

							logger.info("currentAlarm = " + currentAlarm);

							// compare the previous and current alarms,
							// if they are different, save the current alarm into the database
							if (!currentAlarm.equals(previousAlarm)) {

								// write the current alarm to the database (alarms table)
								saveAlarm(currentAlarm);

								logger.info("saved currentAlarm in DB");

								// set the previous alarm to be the current alarm
								previousAlarm = currentAlarm;

							} // end if different alarm

							// read the command from the database (commands table)
							command = getCommand(slaveId);

							/*
							 * Logic for reset and mute:
							 *
							 * old: if reset == 1 then send reset = 1 and set reset = 0 (webapp set reset = 1 on reset button click)
							 * new: if reset == 1 then send reset = 0 and set reset = 0 (webapp set reset = 1 on reset button click)
							 * if mute == 1 then send mute = 0 and set mute = 0 (webapp set mute = 1 on mute button click)
							 */

							// check the reset command
							if (command.isReset()) {  // reset == 1

								logger.info("****** RESET command ******");

//									// set reset = 1 on the holding register
//									resetHoldingRegister[0] = 1;

//								// set reset = 0 on the holding register
//								resetHoldingRegister[0] = 0;

								// write the reset holding register to the slave device
//									modbusSerialDriver.writeHoldingRegisters(
//															slaveId,
//															startAddress - 1 + resetAddressOffset,
//															resetHoldingRegister);
//								writeHoldingRegisters(slaveGroup, slaveId, resetAddressOffset, resetHoldingRegister);
								writeSingleHoldingRegister(slaveGroup, slaveId, resetAddressOffset, ZERO);

								// set the reset = 0 on the command
								command.setReset(false);

								// set the mute = 0 on the command
								command.setMute(false);

								// write the command to the DB
								commandDao.updateCommand(command);

							} // end if reset

							// check the mute command
							if (command.isMute()) {  // mute == 1

								logger.info("****** MUTE command ******");

//								// set mute = 0 on the holding register
//								muteHoldingRegister[0] = 0;

								// write the mute holding register to the slave device
//									modbusSerialDriver.writeHoldingRegisters(
//															slaveId,
//															startAddress - 1 + muteAddressOffset,
//															muteHoldingRegister);
//								writeHoldingRegisters(slaveGroup, slaveId, muteAddressOffset, muteHoldingRegister);
								writeSingleHoldingRegister(slaveGroup, slaveId, muteAddressOffset, ZERO);

								// new logic: mute will be reset by the reset command
//								// set the mute = 0 on the command
//								command.setMute(false);
//
//								// write the command to the DB
//								commandDao.updateCommand(command);

							} // end if reset

						} catch (Exception e) {

							logger.error("Error while processing slave id = " + slaveId);
							logError(slaveGroup, e);

						} // end catch

						// sleep for the configured time
						Thread.sleep(sleepTime);

					} // end for each device
					
//					} // end if serial connection
					
				} catch (Exception e) {

					logError(slaveGroup, e);

				} // end catch
				
			} // end for each slave group
					
		} // end while (true)
		
	} // end execute


	private int[] readHoldingRegisters(SlaveGroup slaveGroup, Byte slaveId) {
		int[] holdingRegisters = null;
		String connectionType = slaveGroup.getConnectionType();
		if (connectionType.equals("serial")) {
			holdingRegisters = slaveGroup.getModbusSerialDriver().readHoldingRegisters(
					slaveId,
					startAddress,
					numberOfRegisters);
		} else if (connectionType.equals("tcp")) {
			holdingRegisters = slaveGroup.getModbusTcpDriver().readHoldingRegisters(
					slaveId,
					startAddress,
					numberOfRegisters);
		} else {
			logger.error("unknown connection type: " + connectionType);
		}
		return holdingRegisters;
	}


	private void writeHoldingRegisters(SlaveGroup slaveGroup, Byte slaveId, int addressOffset, int[] holdingRegisters) {
		String connectionType = slaveGroup.getConnectionType();
		if (connectionType.equals("serial")) {
			slaveGroup.getModbusSerialDriver().writeHoldingRegisters(
					slaveId,
					startAddress + addressOffset,
					holdingRegisters);
		} else if (connectionType.equals("tcp")) {
			slaveGroup.getModbusTcpDriver().writeHoldingRegisters(
					slaveId,
					startAddress + addressOffset,
					holdingRegisters);
		} else {
			logger.error("unknown connection type: " + connectionType);
		}
	}


	private void writeSingleHoldingRegister(SlaveGroup slaveGroup, Byte slaveId, int addressOffset, int holdingRegister) {
		String connectionType = slaveGroup.getConnectionType();
		if (connectionType.equals("serial")) {
			logger.error("Not implemented yet, connection type: " + connectionType);
		} else if (connectionType.equals("tcp")) {
			slaveGroup.getModbusTcpDriver().writeSingleRegister(
					slaveId,
					startAddress + addressOffset,
					holdingRegister);
		} else {
			logger.error("unknown connection type: " + connectionType);
		}
	}


	private void logError(SlaveGroup slaveGroup, Exception e) {
		String connectionType = slaveGroup.getConnectionType();
		if (connectionType.equals("serial")) {
			logger.error("Error while processing slave group for serial port: " +
					slaveGroup.getModbusSerialDriver().getSerialParameters().getPortName(), e);
		} else if (connectionType.equals("tcp")) {
			logger.error("Error while processing slave group for tcp port: " +
					slaveGroup.getModbusTcpDriver().getSlaveIpAddress() + ":" + slaveGroup.getModbusTcpDriver().getSlavePort(), e);
		} else {
			logger.error("unknown connection type: " + connectionType, e);
		}
//		logger.error(e.getMessage());
//		logger.error(e);
//		e.printStackTrace();
	}


	private boolean isSerialPort(String port) {

		return port.startsWith("/dev/");

	}


	private String getConnectionType(Device device) {

		String serialPort = device.getSerialPort();
		String ipAddress = device.getIpAddress();
		String ipPort = device.getIpPort();

		if (isSerialPort(serialPort)) {
			return "serial";
		} else if (inetAddressValidator.isValid(ipAddress) && ipPort != null && !ipPort.isEmpty()) {
			return "tcp";
		} else {
			return "unknown";
		}


	}


	/**
	 * createAlarm
	 * 
	 * @param slaveId
	 * @param holdingRegisters
	 * @return
	 */
	private Alarm createAlarm(int slaveId, int[] holdingRegisters) {
		
		// create a new alarm object
		Alarm alarm = new Alarm();
		
		// fill all the alarm attributes except:
		//  - the data id, which will be set by the database automatically
		//  - the timestamp, which will be set by the dao storeAlarm method
		alarm.setSlaveId((byte) slaveId);
		
		alarm.setGreen_01((holdingRegisters[0] & 0x01) == 0x01);
		alarm.setGreen_02((holdingRegisters[0] & 0x02) == 0x02);
		alarm.setGreen_03((holdingRegisters[0] & 0x04) == 0x04);
		alarm.setGreen_04((holdingRegisters[0] & 0x08) == 0x08);
		alarm.setGreen_05((holdingRegisters[0] & 0x10) == 0x10);
		alarm.setGreen_06((holdingRegisters[0] & 0x20) == 0x20);
		alarm.setGreen_07((holdingRegisters[0] & 0x40) == 0x40);
		alarm.setGreen_08((holdingRegisters[0] & 0x80) == 0x80);
		alarm.setGreen_09((holdingRegisters[1] & 0x01) == 0x01);
		alarm.setGreen_10((holdingRegisters[1] & 0x02) == 0x02);
		alarm.setGreen_11((holdingRegisters[1] & 0x04) == 0x04);
		alarm.setGreen_12((holdingRegisters[1] & 0x08) == 0x08);
		alarm.setGreen_13((holdingRegisters[1] & 0x10) == 0x10);
		alarm.setGreen_14((holdingRegisters[1] & 0x20) == 0x20);
		alarm.setGreen_15((holdingRegisters[1] & 0x40) == 0x40);
		alarm.setGreen_16((holdingRegisters[1] & 0x80) == 0x80);
		alarm.setGreen_17((holdingRegisters[2] & 0x01) == 0x01);
		alarm.setGreen_18((holdingRegisters[2] & 0x02) == 0x02);
		alarm.setGreen_19((holdingRegisters[2] & 0x04) == 0x04);
		alarm.setGreen_20((holdingRegisters[2] & 0x08) == 0x08);
		alarm.setGreen_21((holdingRegisters[2] & 0x10) == 0x10);
		alarm.setGreen_22((holdingRegisters[2] & 0x20) == 0x20);
		alarm.setGreen_23((holdingRegisters[2] & 0x40) == 0x40);
		alarm.setGreen_24((holdingRegisters[2] & 0x80) == 0x80);
		alarm.setGreen_25((holdingRegisters[3] & 0x01) == 0x01);
		alarm.setGreen_26((holdingRegisters[3] & 0x02) == 0x02);
		alarm.setGreen_27((holdingRegisters[3] & 0x04) == 0x04);
		alarm.setGreen_28((holdingRegisters[3] & 0x08) == 0x08);
		alarm.setGreen_29((holdingRegisters[3] & 0x10) == 0x10);
		alarm.setGreen_30((holdingRegisters[3] & 0x20) == 0x20);
		
		alarm.setRed_01((holdingRegisters[4] & 0x01) == 0x01);
		alarm.setRed_02((holdingRegisters[4] & 0x02) == 0x02);
		alarm.setRed_03((holdingRegisters[4] & 0x04) == 0x04);
		alarm.setRed_04((holdingRegisters[4] & 0x08) == 0x08);
		alarm.setRed_05((holdingRegisters[4] & 0x10) == 0x10);
		alarm.setRed_06((holdingRegisters[4] & 0x20) == 0x20);
		alarm.setRed_07((holdingRegisters[4] & 0x40) == 0x40);
		alarm.setRed_08((holdingRegisters[4] & 0x80) == 0x80);
		alarm.setRed_09((holdingRegisters[5] & 0x01) == 0x01);
		alarm.setRed_10((holdingRegisters[5] & 0x02) == 0x02);
		alarm.setRed_11((holdingRegisters[5] & 0x04) == 0x04);
		alarm.setRed_12((holdingRegisters[5] & 0x08) == 0x08);
		alarm.setRed_13((holdingRegisters[5] & 0x10) == 0x10);
		alarm.setRed_14((holdingRegisters[5] & 0x20) == 0x20);
		alarm.setRed_15((holdingRegisters[5] & 0x40) == 0x40);
		alarm.setRed_16((holdingRegisters[5] & 0x80) == 0x80);
		alarm.setRed_17((holdingRegisters[6] & 0x01) == 0x01);
		alarm.setRed_18((holdingRegisters[6] & 0x02) == 0x02);
		alarm.setRed_19((holdingRegisters[6] & 0x04) == 0x04);
		alarm.setRed_20((holdingRegisters[6] & 0x08) == 0x08);
		alarm.setRed_21((holdingRegisters[6] & 0x10) == 0x10);
		alarm.setRed_22((holdingRegisters[6] & 0x20) == 0x20);
		alarm.setRed_23((holdingRegisters[6] & 0x40) == 0x40);
		alarm.setRed_24((holdingRegisters[6] & 0x80) == 0x80);
		alarm.setRed_25((holdingRegisters[7] & 0x01) == 0x01);
		alarm.setRed_26((holdingRegisters[7] & 0x02) == 0x02);
		alarm.setRed_27((holdingRegisters[7] & 0x04) == 0x04);
		alarm.setRed_28((holdingRegisters[7] & 0x08) == 0x08);
		alarm.setRed_29((holdingRegisters[7] & 0x10) == 0x10);
		alarm.setRed_30((holdingRegisters[7] & 0x20) == 0x20);
		
		alarm.setAnom_01((holdingRegisters[8] & 0x01) == 0x01);
		alarm.setAnom_02((holdingRegisters[9] & 0x01) == 0x01);
		alarm.setAnom_03((holdingRegisters[10] & 0x01) == 0x01);
		alarm.setAnom_04((holdingRegisters[11] & 0x01) == 0x01);
		
		alarm.setReset((holdingRegisters[12] & 0x01) == 0x01);
		alarm.setMute((holdingRegisters[13] & 0x01) == 0x01);
		
		// return the created and filled alarm object
		return alarm;

	} // end createAlarm


	/**
	 * saveAlarm
	 * 
	 * @param alarm
	 */
	private void saveAlarm(Alarm alarm) {
		
		// store the alarm into the database
		alarmDao.storeAlarm(alarm);
		
	} // end saveAlarm
	
	
	private Command getCommand(int slaveId) {
		
		// find the command for this slave id
		return commandDao.findCommandBySlaveId((byte) slaveId); 
		
	} // end getCommand

	
	/**
	 * Setter and Getters
	 */

	public IModbusSerialDriver getModbusSerialDriver() {
		return modbusSerialDriver;
	}


	public void setModbusSerialDriver(IModbusSerialDriver modbusSerialDriver) {
		this.modbusSerialDriver = modbusSerialDriver;
	}

	public IModbusTcpDriver getModbusTcpDriver() {
		return modbusTcpDriver;
	}

	public void setModbusTcpDriver(IModbusTcpDriver modbusTcpDriver) {
		this.modbusTcpDriver = modbusTcpDriver;
	}

	public IAlarmDao getAlarmDao() {
		return alarmDao;
	}


	public void setAlarmDao(IAlarmDao alarmDao) {
		this.alarmDao = alarmDao;
	}


	public ICommandDao getCommandDao() {
		return commandDao;
	}


	public void setCommandDao(ICommandDao commandDao) {
		this.commandDao = commandDao;
	}


	public IDeviceDao getDeviceDao() {
		return deviceDao;
	}


	public void setDeviceDao(IDeviceDao deviceDao) {
		this.deviceDao = deviceDao;
	}


	public int getMasterId() {
		return masterId;
	}


	public void setMasterId(int masterId) {
		this.masterId = masterId;
	}

	
	public int getStartAddress() {
		return startAddress;
	}


	public void setStartAddress(int startAddress) {
		this.startAddress = startAddress;
	}


	public int getNumberOfRegisters() {
		return numberOfRegisters;
	}


	public void setNumberOfRegisters(int numberOfRegisters) {
		this.numberOfRegisters = numberOfRegisters;
	}


	public long getSleepTime() {
		return sleepTime;
	}


	public void setSleepTime(long sleepTime) {
		this.sleepTime = sleepTime;
	}
	

} // end AlarmLogger
