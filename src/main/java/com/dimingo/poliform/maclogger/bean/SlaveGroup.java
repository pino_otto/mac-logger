package com.dimingo.poliform.maclogger.bean;

import java.util.List;

import com.dimingo.modbus.hao.serial.IModbusSerialDriver;
import com.dimingo.modbus.hao.tcp.IModbusTcpDriver;
import com.dimingo.poliform.mac.commons.bean.Device;


public class SlaveGroup {

	private String connectionType;	// it can be: "serial" or "tcp"
	
	private IModbusSerialDriver modbusSerialDriver;

	private IModbusTcpDriver modbusTcpDriver;

	private List<Device> deviceList;
	
	/*
	 * Getters and Setters
	 */

	public String getConnectionType() {
		return connectionType;
	}

	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	public IModbusSerialDriver getModbusSerialDriver() {
		return modbusSerialDriver;
	}

	public void setModbusSerialDriver(IModbusSerialDriver modbusSerialDriver) {
		this.modbusSerialDriver = modbusSerialDriver;
	}

	public IModbusTcpDriver getModbusTcpDriver() {
		return modbusTcpDriver;
	}

	public void setModbusTcpDriver(IModbusTcpDriver modbusTcpDriver) {
		this.modbusTcpDriver = modbusTcpDriver;
	}

	public List<Device> getDeviceList() {
		return deviceList;
	}

	public void setDeviceList(List<Device> deviceList) {
		this.deviceList = deviceList;
	}
	
	
} // end SlaveGroup
