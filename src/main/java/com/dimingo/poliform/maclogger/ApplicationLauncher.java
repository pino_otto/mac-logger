package com.dimingo.poliform.maclogger;

import net.wimpi.modbus.util.SerialParameters;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationLauncher {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// load the spring application context
		ClassPathXmlApplicationContext applicationContext = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
	    
		// get the alarm logger bean
		AlarmLogger alarmLogger = 
				(AlarmLogger) applicationContext.getBean("alarmLogger");
		
		// run the alarm logger
		alarmLogger.execute();
	    
	} // end main

} // end class
